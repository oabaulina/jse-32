package ru.baulina.tm.api.endpoint;

import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.UserDTO;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import java.util.List;

public interface IAdminUserEndpoint {

    @WebMethod
    List<UserDTO> getUserList(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    UserDTO findUserById(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    UserDTO findUserByLogin(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    void removeUserById(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id
    );

    @WebMethod
    void removeUserByLogin(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    void lockUserLogin(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    void unlockUserLogin(
            @WebParam(name = "session", partName = "session") SessionDTO session,
            @WebParam(name = "login", partName = "login") String login
    );

    @WebMethod
    String getHost(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

    @WebMethod
    Integer getPort(
            @WebParam(name = "session", partName = "session") SessionDTO session
    );

}
