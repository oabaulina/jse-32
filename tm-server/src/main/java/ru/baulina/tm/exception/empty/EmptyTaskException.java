package ru.baulina.tm.exception.empty;

public class EmptyTaskException extends RuntimeException {

    public EmptyTaskException() {
        super("Error! Task is empty...");
    }

}
