package ru.baulina.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import ru.baulina.tm.api.service.IEntityManagerFactoryService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

@Configuration
@ComponentScan("ru.baulina.tm")
public class ApplicationConfig {

    @Bean
    @NotNull
    @Scope("singleton")
    public EntityManagerFactory entityManagerFactory(
            @NotNull final IEntityManagerFactoryService entityManagerFactoryService
    ) {
        return entityManagerFactoryService.getEntityManagerFactory();
    };

    @Bean
    @NotNull
    @Scope("prototype")
    public EntityManager entityManager(
            @NotNull final EntityManagerFactory entityManagerFactory
    ) {
        return entityManagerFactory.createEntityManager();
    }

}
