package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.baulina.tm.api.endpoint.IAdminUserEndpoint;
import ru.baulina.tm.api.service.IPropertyService;
import ru.baulina.tm.api.service.ISessionService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.UserDTO;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@Controller
public class AdminUserEndpoint extends AbstractEndpoint implements IAdminUserEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private IPropertyService propertyService;

    @Override
    @WebMethod
    public List<UserDTO> getUserList(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        @Nullable final List<User> userList = userService.findAll();
        @Nullable List<UserDTO> userListDTO = new ArrayList<>();
        userList.forEach((user) -> userListDTO.add((new UserDTO()).userDTOfrom(user)));
        return userListDTO;
    }

    @Override
    @WebMethod
    public UserDTO findUserById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") final Long id
    ) {
        sessionService.validate(session, Role.ADMIN);
        @Nullable final User user = userService.findById(id);
        @Nullable UserDTO userDTO = new UserDTO();
        return userDTO.userDTOfrom(user);
    }

    @Override
    @WebMethod
    public UserDTO findUserByLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        @Nullable final User user = userService.findByLogin(login);
        @Nullable UserDTO userDTO = new UserDTO();
        return userDTO.userDTOfrom(user);
    }

    @Override
    @WebMethod
    public void removeUserById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") final Long id
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.removeById(id);
    }

    @Override
    @WebMethod
    public void removeUserByLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.removeByLogin(login);
    }

    @Override
    @WebMethod
    public void lockUserLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.lockUserLogin(login);
    }

    @Override
    @WebMethod
    public void unlockUserLogin(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "login", partName = "login") final String login
    ) {
        sessionService.validate(session, Role.ADMIN);
        userService.unlockUserLogin(login);
    }

    @Override
    @WebMethod
    public String getHost(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        return propertyService.getServiceHost();
    }

    @Override
    @WebMethod
    public Integer getPort(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session, Role.ADMIN);
        return propertyService.getServicePort();
    }
}
