package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.baulina.tm.api.endpoint.ISessionEndpoint;
import ru.baulina.tm.api.service.ISessionService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.dto.UserDTO;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

@WebService
@Controller
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IUserService userService;

    @Override
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login") final String login,
            @WebParam(name = "password", partName = "password") final String password
    ) {
        @Nullable Session session = sessionService.open(login, password);
        @Nullable SessionDTO sessionDTO = new SessionDTO();
        return sessionDTO.sessionDTOfrom(session);
    }

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.close(session);
    }

    @Override
    @WebMethod
    public void closeAllSession(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.closeAll(session);
    }

    @Override
    @WebMethod
    public UserDTO getUser(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        @Nullable User user = userService.findById(session.getUserId());
        @Nullable UserDTO userDTO = new UserDTO();
        return Objects.requireNonNull(userDTO.userDTOfrom(user));
    }

    @Override
    @WebMethod
    public List<Session> getListSession(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        return sessionService.getListSession(session);
    }

}
