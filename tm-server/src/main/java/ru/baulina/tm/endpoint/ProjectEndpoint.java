package ru.baulina.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.baulina.tm.api.endpoint.IProjectEndpoint;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.api.service.ISessionService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.ProjectDTO;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@Controller
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @Nullable
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    public List<ProjectDTO> getProjectList() {
        @Nullable final List<Project> projectList = projectService.findAll();
        @Nullable List<ProjectDTO> projectListDTO = new ArrayList<>();
        projectList.forEach((project) -> projectListDTO.add((new ProjectDTO()).projectDTOfrom(project)));
        return projectListDTO;
    }

    @Override
    @WebMethod
    public void loadProjects(
            @WebParam(name = "projects", partName = "projects") List<Project> projects
    ) {
        projectService.load(projects);
    }

    @Override
    @WebMethod
    public ProjectDTO createProject(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        @Nullable final User user = userService.findById(session.getUserId());
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project =  projectService.create(user, name);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public ProjectDTO createProjectWithDescription(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        @Nullable final User user = userService.findById(session.getUserId());
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project = projectService.create(user, name, description);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "project", partName = "project") Project project
    ) {
        sessionService.validate(session);
        projectService.remove(project);
    }

    @Override
    @WebMethod
    public List<ProjectDTO> findAllProjects(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        @Nullable final Long userId = session.getUserId();
        @Nullable final List<Project> projectList = projectService.findAll(userId);
        @Nullable List<ProjectDTO> tprojectListDTO = new ArrayList<>();
        projectList.forEach((project) -> tprojectListDTO.add((new ProjectDTO()).projectDTOfrom(project)));
        return tprojectListDTO;
    }

    @Override
    @WebMethod
    public void clearProjects(
            @WebParam(name = "session", partName = "session") final SessionDTO session
    ) {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public ProjectDTO findOneProjectById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        sessionService.validate(session);
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project = projectService.findOneById(session.getUserId(), id);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public ProjectDTO findOneProjectByIndex(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        sessionService.validate(session);
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project = projectService.findOneByIndex(session.getUserId(), index);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public ProjectDTO findOneProjectByName(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        @Nullable ProjectDTO projectDTO = new ProjectDTO();
        @Nullable final Project project = projectService.findOneByName(session.getUserId(), name);
        return projectDTO.projectDTOfrom(project);
    }

    @Override
    @WebMethod
    public void removeOneProjectById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id
    ) {
        sessionService.validate(session);
        projectService.removeOneById(session.getId(), id);
    }

    @Override
    @WebMethod
    public void removeOneProjectByIndex(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "index", partName = "index") Integer index
    ) {
        sessionService.validate(session);
        projectService.removeOneByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeOneProjectByName(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "name", partName = "name") String name
    ) {
        sessionService.validate(session);
        projectService.removeOneByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "id", partName = "id") Long id,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        projectService.updateProjectById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session", partName = "session") final SessionDTO session,
            @WebParam(name = "index", partName = "index") Integer index,
            @WebParam(name = "name", partName = "name") String name,
            @WebParam(name = "description", partName = "description") String description
    ) {
        sessionService.validate(session);
        projectService.updateProjectByIndex(session.getUserId(), index, name, description);
    }

}
