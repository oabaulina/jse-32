package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import ru.baulina.tm.api.repository.ISessionRepository;
import ru.baulina.tm.api.service.IPropertyService;
import ru.baulina.tm.api.service.ISessionService;
import ru.baulina.tm.api.service.IUserService;
import ru.baulina.tm.dto.SessionDTO;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.enumerated.Role;
import ru.baulina.tm.exception.AccessDeniedException;
import ru.baulina.tm.exception.empty.EmptyLoginException;
import ru.baulina.tm.exception.empty.EmptyPasswordException;
import ru.baulina.tm.exception.session.SessionTimeOutException;
import ru.baulina.tm.util.HashUtil;
import ru.baulina.tm.util.SignatureUtil;

import javax.persistence.EntityManager;
import java.util.List;

@Service
public final class SessionService extends AbstractService<Session> implements ISessionService {

    @Nullable
    @Autowired
    private IUserService userService;

    @Nullable
    @Autowired
    private IPropertyService propertyService;


    @Nullable
    @Autowired
    protected ApplicationContext context;

    @Nullable
    @Autowired
    protected EntityManager entityManager;

    @NotNull
    @Override
    public ISessionRepository getRepository() {
        return context.getBean(ISessionRepository.class);
    }

    @Nullable
    @Override
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check) throw new AccessDeniedException();
        assert userService != null;
        final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        @Nullable final ISessionRepository repository = getRepository();
        try {
            repository.beginTransaction();
            final Session session = new Session();
            session.setUser(user);
            session.setTimestamp(System.currentTimeMillis());
            final String signature = sign((new SessionDTO()).sessionDTOfrom(session));
            session.setSignature(signature);
            repository.persist(session);
            repository.commitTransaction();
            return session;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Override
    public void close(@Nullable final SessionDTO session) {
        validate(session);
        @Nullable final ISessionRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            repository.remove(session);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void closeAll(@Nullable final SessionDTO session) {
        validate(session);
        if (session.getUserId() == null) throw new AccessDeniedException();
        @Nullable final ISessionRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            repository.removeByUserId(session.getUserId());
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public boolean isValid(@Nullable final SessionDTO session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void validate(@Nullable final SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessDeniedException();
        if (session.getUserId() == null || session.getUserId() < 0) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        if (isTimeOut(session.getTimestamp())) {
            close(session);
            throw new SessionTimeOutException();
        }
        @Nullable final SessionDTO temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        temp.setSignature(null);
        @Nullable final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(temp);
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!contains(session)) throw new AccessDeniedException();
    }

    private boolean contains(@Nullable final SessionDTO session) {
        boolean contains = false;
        @Nullable final ISessionRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            assert session != null;
            contains = repository.contains(session.getId());
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return contains;
    }

    @Override
    public void validate(@Nullable final SessionDTO session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final Long userId = session.getUserId();
        assert userService != null;
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new AccessDeniedException();
        if (!role.equals((user.getRole()))) throw new AccessDeniedException();
    }

    @Override
    public boolean isTimeOut(@NotNull final Long timeStamp) {
        @NotNull Long currentTime = System.currentTimeMillis();
        int timeOut = 60 * 60 * 1000;
        return ((currentTime - timeStamp) > timeOut);
    }

    @Nullable
    @Override
    public List<Session> getListSession(@Nullable final SessionDTO session) {
        validate(session);
        @Nullable final ISessionRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @NotNull List<Session> listSession = repository.getListSession();
            repository.commitTransaction();
            return listSession;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public String sign(@Nullable final SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        assert propertyService != null;
        @Nullable final String salt = propertyService.getSessionSalt();
        @Nullable final Integer cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        return signature;
    }

    @Override
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        assert userService != null;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        if (user.getLocked()) throw new AccessDeniedException();
        final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new AccessDeniedException();
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public void signOutByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        assert userService != null;
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new AccessDeniedException();
        @NotNull final Long userId = user.getId();
        @Nullable final ISessionRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            repository.removeByUserId(userId);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void signOutByUserId(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new AccessDeniedException();
        @Nullable final ISessionRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            repository.removeByUserId(userId);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

}
