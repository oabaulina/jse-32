package ru.baulina.tm.service;

import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.baulina.tm.api.service.IEntityManagerFactoryService;
import ru.baulina.tm.api.service.IPropertyService;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.Session;
import ru.baulina.tm.entity.Task;
import ru.baulina.tm.entity.User;

import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

@Service
public class EntityManagerFactoryService implements IEntityManagerFactoryService {

    @NotNull
    @Autowired
    private final IPropertyService propertyService;

    @Nullable
    private EntityManagerFactory entityManagerFactory;

    public EntityManagerFactoryService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Override
    public EntityManagerFactory getEntityManagerFactory() {
        if (entityManagerFactory == null) {
            entityManagerFactory = factory(propertyService);
        }
        return entityManagerFactory;
    }

    @NotNull
    @Override
    public EntityManagerFactory factory(@NotNull final IPropertyService propertyService) {
        final Map<String, String> settings = new HashMap<>();
        settings.put(Environment.DRIVER,        propertyService.getJdbcDriver());
        settings.put(Environment.URL,           propertyService.getJdbcUrl());
        settings.put(Environment.USER,          propertyService.getJdbcLogin());
        settings.put(Environment.PASS,          propertyService.getJdbcPassword());
        settings.put(Environment.DIALECT,       propertyService.getJdbcDialect());
        settings.put(Environment.HBM2DDL_AUTO,  "update");
        settings.put(Environment.SHOW_SQL,      "true");
        final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        final StandardServiceRegistry registry = registryBuilder.build();
        final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

}
