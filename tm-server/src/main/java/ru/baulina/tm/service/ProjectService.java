package ru.baulina.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.baulina.tm.api.repository.IProjectRepository;
import ru.baulina.tm.api.service.IProjectService;
import ru.baulina.tm.entity.Project;
import ru.baulina.tm.entity.User;
import ru.baulina.tm.exception.empty.*;
import ru.baulina.tm.exception.entity.ProjectNotFoundException;
import ru.baulina.tm.exception.entity.TaskNotFoundException;
import ru.baulina.tm.exception.incorrect.IncorrectIndexException;

import java.util.List;

@Service
public final class ProjectService extends AbstractService<Project> implements IProjectService {

    @NotNull
    @Override
    protected IProjectRepository getRepository() {
        return context.getBean(IProjectRepository.class);
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final User user,
            @Nullable final String name
    ) {
        if (user == null) throw new EmptyUserException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull final Project project = new Project();
            project.setUser(user);
            project.setName(name);
            repository.merge(project);
            repository.commitTransaction();
            return project;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public Project create(
            @Nullable final User user,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (user == null) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull final Project project = new Project();
            project.setUser(user);
            project.setName(name);
            project.setDescription(description);
            repository.merge(project);
            repository.commitTransaction();
            return project;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Override
    public void clear(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            repository.clear(userId);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final Long userId) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @NotNull List<Project> listProject = repository.findAll(userId);
            repository.commitTransaction();
            return listProject;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public List<Project> findListProjects() {
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @NotNull List<Project> listProject = repository.findListProjects();
            repository.commitTransaction();
            return listProject;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneById(
            @Nullable final Long userId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @Nullable Project project = repository.findOneById(userId, id);
            repository.commitTransaction();
            return project;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByIndex(
            @Nullable final Long userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @Nullable Project project = repository.findOneByIndex(userId, index);
            repository.commitTransaction();
            return project;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Nullable
    @Override
    public Project findOneByName(
            @Nullable final Long userId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @Nullable Project project = repository.findOneByName(userId, name);
            repository.commitTransaction();
            return project;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Override
    public void remove(@Nullable final Project project) {
        if (project == null) throw new EmptyProjectException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            repository.remove(project);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void removeOneById(
            @Nullable final Long userId,
            @Nullable final Long id
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @Nullable Project project = repository.removeOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void removeOneByIndex(
            @Nullable final Long userId,
            @Nullable final Integer index
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @Nullable Project project = repository.removeOneByIndex(userId, index);
            if (project == null) throw new ProjectNotFoundException();
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void removeOneByName(
            @Nullable final Long userId,
            @Nullable final String name
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @Nullable Project project = repository.removeOneByName(userId, name);
            if (project == null) throw new ProjectNotFoundException();
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void updateProjectById(
            @Nullable final Long userId, @Nullable final Long id,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (id == null || id < 0) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            final Project project = findOneById(userId, id);
            if (project == null) throw new ProjectNotFoundException();
            project.setId(id);
            project.setName(name);
            project.setDescription(description);
            repository.merge(project);
            repository.commitTransaction();
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Override
    public void updateProjectByIndex(
            @Nullable final Long userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description
    ) {
        if (userId == null || userId < 0) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @Nullable final IProjectRepository repository = getRepository();
        try {
            repository.beginTransaction();
            assert context != null;
            @Nullable final Project project = findOneByIndex(userId, index);
            if (project == null) throw new TaskNotFoundException();
            project.setName(name);
            project.setDescription(description);
            repository.merge(project);
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

}
