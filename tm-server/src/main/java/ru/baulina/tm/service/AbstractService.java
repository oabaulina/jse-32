package ru.baulina.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import ru.baulina.tm.api.repository.IRepository;
import ru.baulina.tm.api.service.IService;
import ru.baulina.tm.entity.AbstractEntity;

import javax.persistence.EntityManager;
import java.util.List;

@RequiredArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @Nullable
    @Autowired
    protected ApplicationContext context;

    @NotNull
    protected abstract IRepository<E> getRepository();

    @Nullable
    @Override
    public List<E> findAll() {
        @NotNull final IRepository<E> repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull List<E> listEntites = repository.findAll();
            repository.commitTransaction();
            return listEntites;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

    @Override
    public void load(@Nullable List<E> entities) {
        @NotNull final IRepository<E> repository = getRepository();
        try {
            repository.beginTransaction();
            repository.clear();
            repository.merge(entities);
            repository.commitTransaction();
       } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
    }

    @Nullable
    @Override
    public Long count() {
        @NotNull final IRepository<E> repository = getRepository();
        try {
            repository.beginTransaction();
            @NotNull Long count = repository.count();
            repository.commitTransaction();
            return count;
        } catch (@NotNull final Exception e) {
            repository.rollbackTransaction();
        } finally {
            repository.closeTransaction();
        }
        return null;
    }

}
