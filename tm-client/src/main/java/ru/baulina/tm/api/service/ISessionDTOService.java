package ru.baulina.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.baulina.tm.endpoint.SessionDTO;

public interface ISessionDTOService {

    void setSession(@Nullable SessionDTO session);

    @Nullable SessionDTO getSession();

    @Nullable Long getUserId();

}
