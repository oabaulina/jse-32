package ru.baulina.tm.exception.incorrect;

public class IncorrectIndexException extends RuntimeException{

    public IncorrectIndexException(String value) {
        super("Error! This value '" + value + "' is not number...");
    }

    public IncorrectIndexException() {
        super("Error! Index is empty...");
    }

}
