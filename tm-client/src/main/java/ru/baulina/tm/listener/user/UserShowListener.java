package ru.baulina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminUserEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.UserDTO;
import ru.baulina.tm.event.ConsoleEvent;

import java.util.List;

@Component
public class UserShowListener extends AbstractUserListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String name() {
        return "list-users";
    }

    @NotNull
    @Override
    public String description() {
        return "Show users.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@userShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        @Nullable final SessionDTO session = getSession();
        final List<UserDTO> users = adminUserEndpoint.getUserList(session);
        System.out.println("[SHOW_LIST_USERS]");
        int index = 1;
        for (UserDTO user: users) {
            System.out.println(index + ". ");
            System.out.println("LOGIN: " + user.getLogin());
            System.out.println("E-MAIL: " + user.getEmail());
            System.out.println("FEST NAME: " + user.getFirstName());
            System.out.println("LAST NAME: " + user.getLastName());
            index++;
        }
        System.out.println("[OK]");
        System.out.println();
    }

}
