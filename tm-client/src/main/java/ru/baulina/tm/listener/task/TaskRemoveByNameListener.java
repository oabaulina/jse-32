package ru.baulina.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.TaskEndpoint;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class TaskRemoveByNameListener extends AbstractTaskListener {

    @Autowired
    private TaskEndpoint taskEndpoint;

    @NotNull
    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@taskRemoveByNameListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE TASK]");
        @Nullable final String name;
        @Nullable final Long projectId;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER NAME");
            name = TerminalUtil.nextLine();
            System.out.println("ENTER PROJECT ID:");
            projectId = TerminalUtil.nexLong();
        }
        @Nullable final SessionDTO session = getSession();
        taskEndpoint.removeOneTaskByName(session, projectId, name);
        System.out.println("[OK]");
        System.out.println();
    }

}
