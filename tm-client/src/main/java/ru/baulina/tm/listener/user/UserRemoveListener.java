package ru.baulina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.AdminUserEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class UserRemoveListener extends AbstractUserListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "remove-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@userRemoveListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE_USER]");
        @NotNull final String login;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER LOGIN:");
            login = TerminalUtil.nextLine();
        }
        @Nullable final SessionDTO session = getSession();
        adminUserEndpoint.removeUserByLogin(session, login);
        sessionEndpoint.closeAllSession(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
