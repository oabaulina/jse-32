package ru.baulina.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.ProjectEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class ProjectRemoveByNameListener extends AbstractProjectListener {

    @Autowired
    private ProjectEndpoint projectEndpoint;

    @NotNull
    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove project by name.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@projectRemoveByNameListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REMOVE PROJECT]");
        @Nullable final String name;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER NAME");
            name = TerminalUtil.nextLine();
        }
        @Nullable final SessionDTO session = getSession();
        projectEndpoint.removeOneProjectByName(session, name);
        System.out.println("[OK]");
        System.out.println();
    }

}
