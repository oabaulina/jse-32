package ru.baulina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.UserEndpoint;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.util.TerminalUtil;

@Component
public class RegistryListener extends AbstractUserListener {

    @Autowired
    private UserEndpoint userEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "registry";
    }

    @NotNull
    @Override
    public String description() {
        return "Register new users.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@registryListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[REGISTRY]");
        @NotNull final String login;
        @NotNull final String password;
        @NotNull final String email;
        synchronized(TerminalUtil.class) {
            System.out.println("ENTER LOGIN:");
            login = TerminalUtil.nextLine();
            System.out.println("ENTER PASSWORD:");
            password = TerminalUtil.nextLine();
            System.out.println("ENTER E-MAIL:");
            email = TerminalUtil.nextLine();
        }
        @Nullable final SessionDTO session = getSession();
        userEndpoint.createUserWithEmail(session, login, password, email);
        System.out.println("[OK]");
        System.out.println();
    }

}
