package ru.baulina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.listener.AbstractListener;

import java.util.List;
import java.util.Objects;

@Component
public class CommandShowListener extends AbstractListener {

    @Lazy
    @Autowired
    private List<AbstractListener> listeners;

    @NotNull
    @Override
    public String arg() {
        return "-cmd";
    }

    @NotNull
    @Override
    public String name() {
        return "commands";
    }

    @NotNull
    @Override
    public String description() {
        return "Show program commands.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@commandShowListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        commandShow();
    }

    @Async("asyncExecutor")
    @EventListener(condition = "@commandShowListener.arg() == #event.name")
    public void handlerArg(@NotNull final ConsoleEvent event) {
        commandShow();
    }

    private void commandShow() {
        System.out.println("[COMMANDS]");
        listeners.stream()
                .map(AbstractListener::name)
                .filter(Objects::nonNull)
                .forEach(x->System.out.println(x));
        System.out.println();
    }
}
