package ru.baulina.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.endpoint.SessionDTO;
import ru.baulina.tm.endpoint.SessionEndpoint;
import ru.baulina.tm.event.ConsoleEvent;

@Component
public class SessionCloseListener extends AbstractUserListener {

    @Autowired
    private SessionEndpoint sessionEndpoint;

    @NotNull
    @Override
    public String name() {
        return "close-session";
    }

    @NotNull
    @Override
    public String description() {
        return "Close session.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@sessionCloseListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[CLOSE_SESSION]");
        @Nullable final SessionDTO session = getSession();
        sessionEndpoint.closeSession(session);
        System.out.println("[OK]");
        System.out.println();
    }

}
