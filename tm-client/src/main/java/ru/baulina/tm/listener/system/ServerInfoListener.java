package ru.baulina.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import ru.baulina.tm.event.ConsoleEvent;
import ru.baulina.tm.listener.AbstractListener;
import ru.baulina.tm.endpoint.AdminUserEndpoint;
import ru.baulina.tm.endpoint.SessionDTO;

@Component
public class ServerInfoListener extends AbstractListener {

    @Autowired
    private AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Override
    public String arg() {
        return "-iSer";
    }

    @NotNull
    @Override
    public String name() {
        return "server-info";
    }

    @NotNull
    @Override
    public String description() {
        return "Display information about server.";
    }

    @Override
    @Async("asyncExecutor")
    @EventListener(condition = "@serverInfoListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        serverInfo();
    }

    @Async("asyncExecutor")
    @EventListener(condition = "@serverInfoListener.arg() == #event.name")
    public void handlerArg(@NotNull final ConsoleEvent event) {
        serverInfo();
    }

    private void serverInfo() {
        System.out.println("[ABOUT]");
        System.out.println("Olga Baulina");
        System.out.println("golovolomkacom@gmail.com");
        System.out.println("OK");
        System.out.println();
    }

}
